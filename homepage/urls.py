from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.gibranharahap, name='gibranharahap'),
    path('about/', views.about, name='about'),
	path('reminder/', views.reminder, name='reminder'),
	path('schedule/', views.schedule, name='schedule'),
	path('addschedule/', views.addschedule, name ='addschedule'),
    path('resetAll/', views.resetAll, name ='resetAll'),
]